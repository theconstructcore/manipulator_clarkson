#!/usr/bin/env python

import rospy
from visualization_msgs.msg import Marker

"""
header: 
  seq: 272
  stamp: 
    secs: 0
    nsecs:         0
  frame_id: ''
ns: "surface_1_object_1_axes"
id: 0
type: 0
action: 2
pose: 
  position: 
    x: 0.0
    y: 0.0
    z: 0.0
  orientation: 
    x: 0.0
    y: 0.0
    z: 0.0
    w: 0.0
scale: 
  x: 0.0
  y: 0.0
  z: 0.0
color: 
  r: 0.0
  g: 0.0
  b: 0.0
  a: 0.0
lifetime: 
  secs: 0
  nsecs:         0
frame_locked: False
points: []
colors: []
text: ''
mesh_resource: ''
mesh_use_embedded_materials: False
"""


# Tools for grasping

class ObjectFinder(object):

    def __init__(self, dim_x, dim_y, error= 0.02):
        self._dim_x = dim_x
        self._dim_y = dim_y
        self._error = error
        self.detected_object = None
        rospy.Subscriber("/surface_objects", Marker, self.object_callback)
        self.pub = rospy.Publisher('/object_to_grasp', Marker, queue_size=10)
    
    def get_detected_object(self):
        return self.detected_object

    def object_callback(self, msg):

        object_to_grasp_marker = Marker()
        object_found = False
        green_value = msg.color.g
        if green_value == 1.0:
            # Its an object that we can grasp an dnot a surface
            object_dim_x = msg.scale.x
            delta_x = abs(self._dim_x) - abs(object_dim_x) 
            object_dim_y = msg.scale.y
            delta_y = abs(self._dim_y) - abs(object_dim_y)
            # Z values are unreliable
            object_dim_z = msg.scale.z

            if delta_x <= self._error and delta_y <= self._error:
                # We found the Object we were looking for 
                object_to_grasp_marker = msg
                object_to_grasp_marker.color.r = 1.0
                object_to_grasp_marker.color.g = 0.0
                object_to_grasp_marker.color.b = 0.0
                object_to_grasp_marker.color.a = 1.0
                rospy.logwarn("FoundObject>>>>>"+str(object_to_grasp_marker.ns))
                object_found = True
                self.detected_object = object_to_grasp_marker
        
        if object_found:
            self.pub.publish(object_to_grasp_marker)
        else:
            rospy.logerr("NOT FOUND Object>>>>>")


if __name__ == "__main__":

    rospy.init_node('object_finder', anonymous=True)
    cube_side = 0.04
    object_finder = ObjectFinder(dim_x=cube_side, dim_y=cube_side)
    rospy.spin()
    
