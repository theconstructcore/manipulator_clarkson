#!/usr/bin/env python

import sys
import rospy
import moveit_commander
from math import pi
from my_moveit_planner.srv import JointStatesValidationServiceMessage, JointStatesValidationServiceMessageResponse


class MoveGroupSafeWatcher(object):

    def __init__(self, init_validation=True):
        super(MoveGroupSafeWatcher, self).__init__()

        ## BEGIN_SUB_TUTORIAL setup
        ## First initialize `moveit_commander`_ and a `rospy`_ node:
        moveit_commander.roscpp_initialize(sys.argv)

        ## Instantiate a `RobotCommander`_ object. This object is the outer-level interface to
        ## the robot:
        self.robot = moveit_commander.RobotCommander()

        ## Instantiate a `PlanningSceneInterface`_ object.  This object is an interface
        ## to the world surrounding the robot:
        self.scene = moveit_commander.PlanningSceneInterface()

        ## Instantiate a `MoveGroupCommander`_ object.  This object is an interface
        group_name = "arm"
        rospy.logwarn("Waiting MAX 5 minutes >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
        self.group = moveit_commander.MoveGroupCommander(group_name, wait_for_servers=300.0)
        rospy.logwarn("Waiting MAX 5 minutes DONE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")

        # Create Service Server
        joint_states_validation_server = rospy.Service('joint_state_validator', JointStatesValidationServiceMessage , self.server_callback) # create the Service called my_service with the defined callback

        if init_validation:
            self.init_moveit_is_working_procedure()
        else:
            rospy.logwarn("Init validation deactivated...")

    def server_callback(self,request):
        
        # We only use the 6 first joints, because the planner only has that
        plan_ok, status_message = self.joint_goal_validation(request.joint_states_data.position[:6])

        response = JointStatesValidationServiceMessageResponse()

        response.valid = plan_ok
        response.status_message = status_message

        return response

    def init_moveit_is_working_procedure(self):

        # We validate with sample examples
        self.plan_only_traj()

        ## Getting Basic Information
        ## ^^^^^^^^^^^^^^^^^^^^^^^^^
        # We can get the name of the reference frame for this robot:
        planning_frame = self.group.get_planning_frame()
        rospy.loginfo("============ Reference frame: %s" % planning_frame)

        # We can also rospy.loginfo the name of the end-effector link for this group:
        eef_link = self.group.get_end_effector_link()
        rospy.loginfo("============ End effector: %s" % eef_link)

        # We can get a list of all the groups in the robot:
        group_names = self.robot.get_group_names()
        rospy.loginfo("============ Robot Groups:"+str(self.robot.get_group_names()))

        # Sometimes for debugging it is useful to rospy.loginfo the entire state of the
        # robot:
        rospy.loginfo("============ rospy.loginfoing robot state")
        rospy.loginfo(self.robot.get_current_state())
        rospy.loginfo ("")

    def joint_goal_validation(self, joint_goal):        

        rospy.loginfo(str(joint_goal))
        if len(joint_goal) < 7:
            rospy.logwarn("The Joint oal is missing elements, we need 7... We will ad them")
            joint_goal_array=list(joint_goal)
            joint_goal_array.append(0.0)
            joint_goal=tuple(joint_goal_array)
            rospy.loginfo("Fixed with 7 joints=="+str(joint_goal))
        
        self.group.set_joint_value_target(joint_goal)
        rospy.loginfo("Starting PLanning...")
        plan_ok = self.plan_trajectory()

        if not plan_ok:
            status_message = "DANGEROUS JOINTS"
            rospy.logerr(status_message)
        else:
            status_message = "OK JOINTS"
            rospy.logwarn(status_message)        

        return plan_ok, status_message
            

    def joint_traj_gen_plan(self):

        joint_goal = self.group.get_current_joint_values()
        rospy.loginfo ("joint_goal"+str(joint_goal))
        
        joint_goal[0] = 0
        joint_goal[1] = -pi/4
        joint_goal[2] = 0
        joint_goal[3] = -pi/2
        joint_goal[4] = 0
        joint_goal[5] = pi/3
        rospy.loginfo ("##################### OK joint_goal"+str(joint_goal))

        return joint_goal

    def joint_traj_gen_error_plan(self):

        joint_goal = self.group.get_current_joint_values()
        
        joint_goal[0] = -1.76
        joint_goal[1] = 2.00
        joint_goal[2] = 0
        joint_goal[3] = 0
        joint_goal[4] = 0
        joint_goal[5] = 0
        rospy.loginfo ("##################### ERROR joint_goal"+str(joint_goal))

        return joint_goal

    def plan_trajectory(self):

        plan = self.group.plan()
        plan_ok = plan[0]
        
        return plan_ok


    def plan_only_traj(self):
        rospy.loginfo(">>>>>>>>>>>>>>>Planning Valid Joint Config")
        joint_goal = self.joint_traj_gen_plan()
        self.joint_goal_validation(joint_goal)

        rospy.loginfo(">>>>>>>>>>>>>>>Planning ERROR Joint Config")
        joint_goal = self.joint_traj_gen_error_plan()
        self.joint_goal_validation(joint_goal)



def main():
    rospy.init_node('joint_state_validator_server',
                    anonymous=True)
    try:
        move_group_watcher_obj = MoveGroupSafeWatcher(init_validation=False)        

        rospy.spin()

    except rospy.ROSInterruptException:
        return
    except KeyboardInterrupt:
        return

if __name__ == '__main__':
    main()