#! /usr/bin/env bash

function kill_all_follow_joint_trajectory_pubisher_nodes {
    echo -e "$BLUETEXT\n kill_all_follow_joint_trajectory_pubisher_nodes"
    set +x
   
    echo "Getting list of Publishers of follow_joint_trajectory..."
    INFO=$(rostopic info /arm/follow_joint_trajectory/goal)
    INFO2=$(echo $INFO | sed -e 's/.*Publishers:\(.*\)Subscribers.*/\1/')
    IFS=' ' read -r -a array <<< $INFO2

    new_array=()
    for value in "${array[@]}"
    do
	[[ ( $value != *"(http://"* ) && ( $value != *".sh"* ) && ( $value != *".py"* ) && ( $value != *"run_traj_control_node"* ) ]] && new_array+=($value)
    done
    array=("${new_array[@]}")
    unset new_array
	
    new_array=()
    for value in "${array[@]}"
    do
        [[ $value == *"/"* ]] && new_array+=($value)
    done
    
    array=("${new_array[@]}")
    unset new_array


    echo "Getting list of Publishers of follow_joint_trajectory...DONE"
    echo "${array[@]}"


    echo "Kill of follow_joint_trajectory publishers nodes..."
    for value in "${array[@]}"
    do
        rosnode kill $value
    done
    echo "Kill of follow_joint_trajectory publishers nodes...DONE"

   
    echo "cleanup of rosnode..."
    # We input yes because sometimes it asks this user input
    yes 2>/dev/null | rosnode cleanup
    echo "cleanup of rosnode...DONE"

    set -x
    echo -e "$BLUETEXT\n kill_all_follow_joint_trajectory_pubisher_nodes...DONE"
    echo -e "\n$WHITETEXT"
}

function main { 
    source /opt/ros/noetic/setup.bash
    source /home/theconstruct/catkin_ws/devel/setup.bash
    # source /home/tgrip/TheConstruct/ros_playground/open_manipulator_v2_ws/devel/setup.bash
    kill_all_follow_joint_trajectory_pubisher_nodes
}

main
