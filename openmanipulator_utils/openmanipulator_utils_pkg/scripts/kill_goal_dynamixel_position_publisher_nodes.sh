#! /usr/bin/env bash

function kil_all_goal_dynamixel_position_pubisher_nodes {
    echo -e "$BLUETEXT\n kil_all_goal_dynamixel_position_pubisher_nodes"
    set +x
   
    echo "Getting list of Publishers of goal_dynamixel_position..."
    INFO=$(rostopic info /goal_dynamixel_position)
    INFO2=$(echo $INFO | sed -e 's/.*Publishers:\(.*\)Subscribers.*/\1/')
    IFS=' ' read -r -a array <<< $INFO2

    new_array=()
    for value in "${array[@]}"
    do
	[[ ( $value != *"(http://"* ) && ( $value != *".sh"* ) && ( $value != *".py"* ) && ( $value != *"move_group_watcher"* )  && ( $value != *"dynamixel_action_server"* ) ]] && new_array+=($value)
    done
    array=("${new_array[@]}")
    unset new_array
	
    new_array=()
    for value in "${array[@]}"
    do
        [[ $value == *"/"* ]] && new_array+=($value)
    done
    
    array=("${new_array[@]}")
    unset new_array


    echo "Getting list of Publishers of goal_dynamixel_position...DONE"
    echo "${array[@]}"


    echo "Kill of goal_dynamixel_position publishers nodes..."
    for value in "${array[@]}"
    do
        rosnode kill $value
    done
    echo "Kill of goal_dynamixel_position publishers nodes...DONE"

   
    echo "cleanup of rosnode..."
    # We input yes because sometimes it asks this user input
    yes 2>/dev/null | rosnode cleanup
    echo "cleanup of rosnode...DONE"

    set -x
    echo -e "$BLUETEXT\n kil_all_goal_dynamixel_position_pubisher_nodes...DONE"
    echo -e "\n$WHITETEXT"
}

function main { 
    source /opt/ros/noetic/setup.bash
    source /home/theconstruct/catkin_ws/devel/setup.bash
    # source /home/tgrip/TheConstruct/ros_playground/open_manipulator_v2_ws/devel/setup.bash
    kil_all_goal_dynamixel_position_pubisher_nodes
}

main
