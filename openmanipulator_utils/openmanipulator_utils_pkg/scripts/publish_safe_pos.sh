#!/usr/bin/env bash

function publish_safe_joint {

    # We stop the head
    source /opt/ros/noetic/setup.bash
    # rostopic pub /cmd_vel sensor_msgs/JointState "{linear:{x: 0.0, y: 0.0, z: 0.0}, angular:{x: 0.0, y: 0.0, z: 0.0}}" --once
    echo "PublishSafePos..."
    rostopic pub /goal_dynamixel_position sensor_msgs/JointState "{header:{seq: 0,stamp:{secs: 0, nsecs: 0},frame_id: ''},name: ['id_1','id_2','id_3','id_4','id_5','id_6','id_7'],position: [0,0,0,0,0,0,0],velocity: [],effort: []}" --once
    echo "PublishSafePos...DONE"

}


function main {
    publish_safe_joint
}

main

